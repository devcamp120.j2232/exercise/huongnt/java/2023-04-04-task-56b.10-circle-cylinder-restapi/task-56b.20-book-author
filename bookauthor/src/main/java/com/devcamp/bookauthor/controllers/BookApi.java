package com.devcamp.bookauthor.controllers;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.bookauthor.models.Author;
import com.devcamp.bookauthor.models.Book;

@RestController
@CrossOrigin
@RequestMapping("/api")

public class BookApi {
    @GetMapping("/books")
    public ArrayList<Book> getBookList(){
        Author author1 = new Author("Margaret Mitchell", "Mitchell@gmail.com", 'f');
        Author author2 = new Author("Conan Doyle",'m');
        Author author3 = new Author("Agatha Christie",'f');

        System.out.println(author1);
        System.out.println(author2);
        System.out.println(author3);

        Book book1 = new Book("Gone with the wind", author1, 200000.0, 1);
        Book book2 = new Book("Sherlock Homes",author2,300000.0, 2);
        Book book3 = new Book("Murder on the Orient Express", author3, 280000.0, 3);

        System.out.println(book1);
        System.out.println(book2);
        System.out.println(book3);

        //tạo danh sách ArrayList Book
        ArrayList<Book> BookList = new ArrayList<>();
        BookList.add(book1);
        BookList.add(book2);
        BookList.add(book3);

        return BookList;
        
        
    }
} 
